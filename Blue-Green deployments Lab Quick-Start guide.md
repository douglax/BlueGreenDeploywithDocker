# Green-Blue Deployments Lab Quick-Start

## Setup environment

### Create and Start Consul machine

```
docker-machine create -d virtualbox consul

export KV_IP=$(docker-machine ssh consul 'ifconfig eth1 | grep "inet addr:" | cut -d: -f2 | cut -d" " -f1')

eval $(docker-machine env consul)

docker run -d \
  -p ${KV_IP}:8500:8500 \
  -h consul \
  --restart always \
  gliderlabs/consul-server -bootstrap
```

### Create and Start Master and Slave machines

```
docker-machine create \
  -d virtualbox \
  --swarm \
  --swarm-master \
  --swarm-discovery="consul://${KV_IP}:8500" \
  --engine-opt="cluster-store=consul://${KV_IP}:8500" \
  --engine-opt="cluster-advertise=eth1:2376" \
  master

export MASTER_IP=$(docker-machine ssh master 'ifconfig eth1 | grep "inet addr:" | cut -d: -f2 | cut -d" " -f1')

docker-machine create \
  -d virtualbox \
  --swarm \
  --swarm-discovery="consul://${KV_IP}:8500" \
  --engine-opt="cluster-store=consul://${KV_IP}:8500" \
  --engine-opt="cluster-advertise=eth1:2376" \
  slave

export SLAVE_IP=$(docker-machine ssh slave 'ifconfig eth1 | grep "inet addr:" | cut -d: -f2 | cut -d" " -f1')
```

### Create registrator containers

```
eval $(docker-machine env master)

docker run -d \
  --name=registrator \
  -h ${MASTER_IP} \
  --volume=/var/run/docker.sock:/tmp/docker.sock \
  gliderlabs/registrator:v6 \
  consul://${KV_IP}:8500

eval $(docker-machine env slave)

docker run -d \
  --name=registrator \
  -h ${SLAVE_IP} \
  --volume=/var/run/docker.sock:/tmp/docker.sock \
  gliderlabs/registrator:v6 \
  consul://${KV_IP}:8500
```

## Verify environment

```
eval $(docker-machine env -swarm master)

$ docker-machine ls
NAME      ACTIVE   DRIVER       STATE     URL                         SWARM             DOCKER        ERRORS
consul    -        virtualbox   Running   tcp://192.168.99.104:2376                     v18.05.0-ce
default   -        virtualbox   Running   tcp://192.168.99.100:2376                     v18.05.0-ce
master    -        virtualbox   Running   tcp://192.168.99.105:2376   master (master)   v18.05.0-ce
slave     *        virtualbox   Running   tcp://192.168.99.106:2376   master            v18.05.0-ce
```

## Test Application

### Start services

```
docker-compose up -d

docker-compose scale blue=3 green=3

docker-machine ls

docker-compose ps

```

### Verify running services 


docker exec bg cat /var/live

Point the browser to the MASTER_IP

http://  :80  Version 1

http://  :8080 Version 2

Check nginx.conf 

docker exec bg cat /etc/nginx/nginx.conf


### Switch the live environment to green

```
docker exec bg switch green

docker exec bg cat /var/live

docker exec bg cat /etc/nginx/nginx.conf
```

Point the browser to the MASTER_IP

http://  :80  Version 1

http://  :8080 Version 2



### Update GreenBlue env version

In the blue service of the docker-compose.yml file, change the line _image: douglax/nginx-html:1_ to _image: douglax/nginx-html:3_. To update the blue service, run the following command.

```
docker-compose up -d blue
```
Switch to blue to verify it

```
docker exec bg switch blue
```


## Stoping and removing containers

```
docker-compose down
docker-machine stop consul master slave
docker-machine rm consul master slave
'''
