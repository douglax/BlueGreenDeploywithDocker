#!/bin/bash

docker-compose down
docker-machine stop consul master slave
docker-machine rm consul master slave